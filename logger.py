import logging
import os
import sys

class Logger(object):

    def __init__(self, dir, name='logger', level=logging.INFO):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(level)
        self.formatter = logging.Formatter(fmt='%(asctime)s [PYTHON] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

        self.fullname = os.path.join(dir, name + ".log")
        self.fh = logging.FileHandler(self.fullname, 'a+')
        self.fh.setFormatter(self.formatter)
        self.logger.addHandler(self.fh)
        self.logger.propagate = False

    def debug(self, msg):
        if self.logger.level >= logging.DEBUG:
            self.logger.debug(msg)

    def info(self, msg):
        if self.logger.level >= logging.INFO:
            self.logger.info(msg)

    def warning(self, msg):
        if self.logger.level >= logging.WARNING:
            self.logger.warning(msg)

    def error(self, msg):
        if self.logger.level >= logging.ERROR:
            self.logger.error(msg)

    