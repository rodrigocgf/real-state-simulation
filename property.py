import sys
import threading
import logger
import os
import time
import random
from random import randint


class Property(object):
    MIN_SELL_PRICE = 210
    MAX_SELL_PRICE = 270
    MIN_RENT_PRICE = 70
    MAX_RENT_PRICE = 110
    EMPTY = 1
    SOLD  = 2
    RENTED  = 3

    def __init__(self, p_id):
        self.Id = p_id
        self.SellPrice = random.randint(self.MIN_SELL_PRICE , self.MAX_SELL_PRICE)
        self.RentPrice = random.randint(self.MIN_RENT_PRICE , self.MAX_RENT_PRICE)
        self.status = self.EMPTY
        self.owner = None
        
    def GetId(self):
        return self.Id

    def GetStatus(self):
        return self.status

    def SetStatus(self, p_Status):
        if p_Status in [self.EMPTY, self.SOLD, self.RENTED]:
            self.status = p_Status
        else:
            raise Exception("Invalid property status.")

    def SetOwner(self, p_Player):
        self.owner = p_Player

    def GetOwner(self):
        if self.owner != None:
            return self.owner
        else:
            return None

    def GetRentPrice(self):
        return self.RentPrice

    def GetSellPrice(self):
        return self.SellPrice