import sys
import os
import threading
from controller import Controller

def main():
    LogsDir = os.path.join( os.getcwd(), "log")
    thrMain = threading.Thread(target = LoopMain, args=(LogsDir,))
    thrMain.start()

def LoopMain(pLogsDir):
    strInput = ""

    controller = Controller(pLogsDir)
    controller.Start()

    while strInput != "quit":
        print("Enter 'quit' to exit: ")
        strInput = sys.stdin.readline()
        strInput = strInput.lstrip().rstrip()

        if ( str(strInput) == "quit"):
            controller.Stop()
    

if __name__ == "__main__":
   sys.exit(main() or 0)