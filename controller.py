import sys
import threading
import logger
import os
import time
from property import Property
import random
from random import randint
from player import Player


class Controller(object):
    MAX_ROUNDS = 1000
    NUM_PROPERTIES = 20
    NUM_SIMULATIONS = 300

    def __init__(self, osLogDir):
        self.OsLogDir = str(osLogDir)
        self.log = logger.Logger(self.OsLogDir,"Controller")
        self.log.propagate = False
   
        self.Players = []
        self.currentPlayer = 0

        self.numSimulationTimeouts = 0
        self.totalNumRounds = 0
        self.numImpulsiveWins = 0
        self.numDemandingWins = 0
        self.numCautiousWins = 0
        self.numRandomWins = 0
        self.totalWins = 0
    
    def Start(self):

        for i in range(1,self.NUM_SIMULATIONS + 1):
            self.ProcessSimulation()

        self.totalWins = self.numImpulsiveWins + self.numDemandingWins + self.numCautiousWins + self.numRandomWins
        avg = int(self.totalNumRounds/self.NUM_SIMULATIONS)

        percentWinsImpusive = 0.00
        percentWinsDemanding = 0.00
        percentWinsCautious = 0.00
        percentWinsRandom = 0.00

        if self.totalWins > 0:
            percentWinsImpusive = self.numImpulsiveWins*100/self.totalWins
            percentWinsDemanding = self.numDemandingWins*100/self.totalWins
            percentWinsCautious = self.numCautiousWins*100/self.totalWins
            percentWinsRandom = self.numRandomWins*100/self.totalWins

        print("===============================================================")
        print(" ")
        print("                 SUMMARY AFTER 300 SIMULATIONS                 ")
        print(" ")
        print("  Simulations ended by TIMEOUT           : " + str(self.numSimulationTimeouts) )
        print("  Average rounds by party                : " + str(avg))
        print("  Percentage of wins by IMPULSIVE player : %.2f" % percentWinsImpusive )
        print("  Percentage of wins by DEMANDING player : %.2f" % percentWinsDemanding  )
        print("  Percentage of wins by CAUTIOUS player  : %.2f" % percentWinsCautious )
        print("  Percentage of wins by RANDOM player    : %.2f" % percentWinsRandom )
        print("===============================================================")
        print(" ")
        print("  Total WINS                             : " + str(self.totalWins))
        print("  Wins by IMPULSIVE player               : " + str(self.numImpulsiveWins))
        print("  Wins by DEMANDING player               : " + str(self.numDemandingWins))
        print("  Wins by CAUTIOUS player                : " + str(self.numCautiousWins))
        print("  Wins by RANDOM player                  : " + str(self.numRandomWins))


    def ProcessSimulation(self):
        self.CreateProperties()
        self.SetNumPlayers()
        self.CreatePlayers()
        self.SetPlayersOrder()
        self.ProcessRounds()


    def Stop(self):
        self.bEnd = True

    def CreateProperties(self):
        self.properties = []
        for i in range(0,self.NUM_PROPERTIES):
            property = Property(i)
            self.properties.append(property)


    def SetNumPlayers(self):
        self.NumPlayers = randint(20,20)
        print("[Controller::SetNumPlayers] Number of players : " + str(self.NumPlayers))

    def CreatePlayers(self):
        self.players = []
        self.currentPlayer = 0
        for i in range(0, self.NumPlayers):
            if i%4 == 0:
                player = Player(Player.IMPULSIVE, i)
                self.Players.append(player)
            elif i%4 == 1:
                player = Player(Player.DEMANDING,i)
                self.Players.append(player)
            elif i%4 == 2:
                player = Player(Player.CAUTIOUS, i)
                self.Players.append(player)
            elif i%4 == 3:
                player = Player(Player.RANDOM,i)
                self.Players.append(player)


    def SetPlayersOrder(self):
        self.playersOrder = random.sample(range(self.NumPlayers), self.NumPlayers)
        print("PLAYERS ORDER : ")
        print(str(self.playersOrder))
        print(" ")

    def GetNextPlayer(self):
        return self.playersOrder[self.currentPlayer%self.NumPlayers]

    def GetDiceNumber(self):
        return random.randint(1,6)

    def ProcessRounds(self):
        count = 0
        for i in range(0,self.MAX_ROUNDS):
            print("-----------------------------------------------")
            print("==> ROUND " + str(i))

            currentPlayerNumber = self.GetNextPlayer()
            print("Player " + str(currentPlayerNumber) + " will play the dice.")
            player = self.Players[currentPlayerNumber]
            playerPosition = player.GetCurrentPosition()

            print("Player " + str(currentPlayerNumber) + " position is " + str(playerPosition))
            numStepsToMove = self.GetDiceNumber()
            print("Dice drawn number : " + str(numStepsToMove))

            playerPosition += numStepsToMove
            if playerPosition > self.NUM_PROPERTIES:
                player.BalanceIncrease(100)

            playerPosition %= self.NUM_PROPERTIES
            player.SetCurrentPosition(playerPosition)

            property = self.properties[playerPosition]
            print("Property Id at player NEW position : " + str(property.GetId()))

            if property.GetStatus() == Property.EMPTY:
                print("Property " + str(property.GetId() ) + " is EMPTY")
                if player.GetBalance() > property.SellPrice :
                    player.Buy(property)
            elif property.GetStatus() == Property.SOLD:
                print("Property " + str(property.GetId() ) + " is SOLD")
                player.Rent(property)
                if player.GetBalance() < 0:
                    print("*********************************************************")
                    print("Player " + str(currentPlayerNumber) + " Will be REMOVED !")
                    self.playersOrder.remove(currentPlayerNumber)
                    print("NEW PLAYERS ORDER : ")
                    print(str(self.playersOrder))
                    print("*********************************************************")
                    self.NumPlayers -= 1
                    print(" ")

            if len(self.playersOrder) == 1:
                break
                    

            self.currentPlayer = self.currentPlayer + 1
            count += 1

        if len(self.playersOrder) == 1:
            winnerPosition =  self.GetNextPlayer()
            print("WINNER Player is " + str(winnerPosition) )
            winner = self.Players[winnerPosition]
            print("WINNER PROFILE : " + str(winner.GetProfile()))

            if winner.GetProfile() == Player.IMPULSIVE:
                print("IMPULSIVE WIN")
                self.numImpulsiveWins += 1
            elif winner.GetProfile() == Player.DEMANDING:
                print("DEMANDING WIN")
                self.numDemandingWins += 1
            elif winner.GetProfile() == Player.CAUTIOUS:
                print("CAUTIOUS WIN")
                self.numCautiousWins += 1
            elif winner.GetProfile() == Player.RANDOM:
                print("RANDOM WIN")
                self.numRandomWins += 1
        else:
            print("SIMULATION ENDED BY TIMOUT !")
            self.numSimulationTimeouts += 1

        self.totalNumRounds += count


