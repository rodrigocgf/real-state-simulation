from abc import ABC, abstractmethod
from strategyinterface import StrategyInterface
from property import Property

class CautiousStrategy(StrategyInterface):

    def __init__(self):
        pass

    def Buy(self, p_Property, p_Buyer):
        print("Player strategy is CAUTIOUS")
        if p_Buyer.GetBalance() - p_Property.GetSellPrice() > 80:

            print("Player " + str(p_Buyer.Id) + " BUYS Property " + str(p_Property.GetId()))
            print("Player " + str(p_Buyer.Id) + " balance before buy = " + str(p_Buyer.GetBalance()) )
            print("Property " + str(p_Property.GetId()) + " sell price = " + str(p_Property.GetSellPrice()))

            p_Buyer.Balance -= p_Property.SellPrice
            p_Property.SetOwner(p_Buyer)
            p_Property.SetStatus(Property.SOLD)

            print("Player " + str(p_Buyer.Id) + " balance after buy = " + str(p_Buyer.GetBalance()) )
            print("Property " + str(p_Property.GetId()) + " SOLD")

    def Rent(self, p_Property, p_Tenant ):

        print("Player " + str(p_Tenant.Id) + " RENTS Property " + str(p_Property.GetId()))
        print("Property " + str(p_Property.GetId()) + " RENT price = " + str(p_Property.GetRentPrice()))
        print("Player (Tenant) " + str(p_Tenant.Id) + " balance before rent = " + str(p_Tenant.GetBalance()) )
        print("Property " + str(p_Property.GetId()) + " OWNER is Player " + str( p_Property.GetOwner().GetId() ) )
        print("Player (Landlord) " + str(p_Property.GetOwner().GetId() ) + " balance before RENT is " + str(p_Property.GetOwner().GetBalance()))

        p_Tenant.Balance -= p_Property.RentPrice
        owner = p_Property.GetOwner()
        owner.BalanceIncrease(p_Property.RentPrice)

        print("Player (Landlord) " + str(p_Property.GetOwner().GetId() ) + " balance after RENT is " + str(p_Property.GetOwner().GetBalance()))
        print("Player (Tenant) " + str(p_Tenant.Id) + " balance after rent = " + str(p_Tenant.GetBalance()) )