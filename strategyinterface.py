from abc import ABC, abstractmethod


class StrategyInterface(ABC):

    @abstractmethod
    def Buy(self , p_Property , p_Buyer):
        pass

    @abstractmethod
    def Rent(self, p_Property , p_Tenant ):
        pass