import sys
import threading
import logger
import os
import time
from strategyinterface import StrategyInterface
from impulsivestrategy import ImpulsiveStrategy
from demandingstrategy import DemandingStrategy
from cautiousstrategy  import CautiousStrategy
from randomstrategy import RandomStrategy
from property import Property


class Player():
    IMPULSIVE = 1
    DEMANDING = 2
    CAUTIOUS = 3
    RANDOM = 4

    def __init__(self, p_Profile, p_id):
        if p_Profile in [self.IMPULSIVE, self.DEMANDING, self.CAUTIOUS, self.RANDOM]:
            self.Profile = p_Profile

        self.Id = p_id
        self.Balance = 300
        self.SetStrategy()
        self.currentPosition = 1

    def GetCurrentPosition(self):
        return self.currentPosition

    def SetCurrentPosition(self, p_pos):
        self.currentPosition = p_pos

    def GetId(self):
        return self.Id

    def SetStrategy(self):
        if self.Profile == self.IMPULSIVE:
            self.Strategy = ImpulsiveStrategy()
        elif self.Profile == self.DEMANDING:
            self.Strategy = DemandingStrategy()
        elif self.Profile == self.CAUTIOUS:
            self.Strategy = CautiousStrategy()
        elif self.Profile == self.RANDOM:
            self.Strategy = RandomStrategy()
        else:
            raise Exception("Invalid Strategy.")

    def GetProfile(self):
        return self.Profile

    def Buy(self, p_Property):
        #print("Player " + str(self.Id) + " Buys Property " + str(p_Property.GetId()))
        #print("Player " + str(self.Id) + " balance before buy = " + str(self.GetBalance()) )
        #print("Property " + str(p_Property.GetId()) + " sell price = " + str(p_Property.GetSellPrice()))
        self.Strategy.Buy(p_Property, self)
        #print("Player " + str(self.Id) + " balance after buy = " + str(self.GetBalance()) )
        

    def Rent(self, p_Property):
        #print("Player " + str(self.Id) + " Rents Property " + str(p_Property.GetId()))
        #print("Player " + str(self.Id) + " balance before rent = " + str(self.GetBalance()) )
        #print("Property " + str(p_Property.GetId()) + " rent price = " + str(p_Property.GetRentPrice()))
        self.Strategy.Rent( p_Property, self )
        #print("Player " + str(self.Id) + " balance after rent = " + str(self.GetBalance()) )

    def BalanceDecrease(self, p_value):
        self.Balance -= p_value

    def BalanceIncrease(self, p_value):
        self.Balance += p_value

    def GetBalance(self):
        return self.Balance
